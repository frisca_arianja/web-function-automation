from math import floor


def convert_to_elapsed_time(seconds: float) -> str:
    elapsed = ''
    time_unit_abbr = ["s", "m", "h"]
    time_unit = {"s": 60, "m": 60, "h": 24}

    div_res = seconds
    for unit in time_unit_abbr:
        if div_res >= time_unit[unit]:
            div_res, mod_res = divmod(div_res, time_unit[unit])
            elapsed = '{}{}'.format(floor(mod_res), unit) + ' ' + elapsed
        else:
            elapsed = '{}{}'.format(floor(div_res), unit) + ' ' + elapsed
            div_res = 0
            break

    if floor(div_res) > 0:
        elapsed = '{}d'.format(floor(div_res)) + ' ' + elapsed

    return elapsed[:-1]