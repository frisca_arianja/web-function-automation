from typing import Dict, Any
from requests import post
from json import loads, dumps


class AuthError(Exception):
    pass


class grant_types:
    PRIVATE_ACCESS_TOKEN = "private_access_token"
    PASSWORD = "password"


class Authorization:
    def __init__(self, grant_type: str, auth_data: Dict[str, str]):
        self.__grant_type = grant_type

        if grant_type == grant_types.PRIVATE_ACCESS_TOKEN:
            self.__token = auth_data.get("token", None)

            if not self.__token:
                raise AuthError("Token missing")
        elif grant_type == grant_types.PASSWORD:
            username = auth_data.get("username", None)
            password = auth_data.get("password", None)

            if not username or not password:
                raise AuthError("Username and password are mandatory")

            oauth_resp = self.ouath_authorization(username, password)

            self.__access_token = oauth_resp["access_token"]
            self.__refresh_token = oauth_resp["refresh_token"]
        else:
            raise Exception("Not supported")

    @property
    def token(self) -> str:
        if self.__grant_type != grant_types.PRIVATE_ACCESS_TOKEN:
            raise Exception("Token not found")

        return self.__token

    @property
    def access_token(self) -> str:
        if self.__grant_type != grant_types.PASSWORD:
            raise Exception("Access token not found")

        return self.__access_token

    @property
    def refresh_token(self) -> str:
        if self.__grant_type != grant_types.PASSWORD:
            raise Exception("Refresh token not found")

        return self.__refresh_token

    @property
    def header(self):
        if self.__grant_type == grant_types.PRIVATE_ACCESS_TOKEN:
            return {"Private-Token": self.token}
        elif self.__grant_type == grant_types.PASSWORD:
            return {"Authorization": "Bearer {}".format(self.access_token)}

    def ouath_authorization(self, username: str, password: str) -> Dict[str, Any]:
        data = {
            "grant_type": "password",
            "username": username,
            "password": password
        }

        resp = post("https://www.gitlab.com/oauth/token", data=dumps(data),
                    headers={"Content-Type": "application/json"})

        if resp.status_code != 200:
            raise AuthError(resp.content["error_description"])

        return loads(resp.content)
