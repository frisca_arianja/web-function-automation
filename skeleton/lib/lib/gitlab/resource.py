from typing import Dict, Any, Callable, Union, TYPE_CHECKING

if TYPE_CHECKING:
    from .options import Options


class Resource:
    def __init__(self, options: 'Options', path: str, params: Dict[str, Any]):
        self.__params = self.params_changer(params)
        self.__path = "/{}".format(path)
        self.__options = options

    @property
    def params(self) -> str:
        return self.__params

    @property
    def url(self) -> str:
        return self.__options.url

    @property
    def authorization(self) -> Dict[str, Any]:
        return self.__options.authorization

    @property
    def path(self) -> str:
        return self.__path

    @params.setter
    def params(self, params: Union[Dict[str, str], str]):
        if type(params) is str:
            self.__params = params
        else:
            self.__params = self.params_changer(params)

    @path.setter
    def path(self, path: str):
        self.__path = path

    def params_changer(self, params: Dict[str, Any] = {}) -> str:
        params_str = ""

        if params:
            params_str = "?"

        value = ""
        for param in params:
            if type(params[param]) is list:
                for val in params[param]:
                    value += "{},".format(val)

                value = value[:-1]
            else:
                value = params[param]

            params_str += "{}={}&".format(param, value)

        return params_str[:-1]
