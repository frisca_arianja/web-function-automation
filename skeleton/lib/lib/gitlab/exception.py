class NotFound(Exception):
    pass


class Unauthorized(Exception):
    pass


class BadRequest(Exception):
    pass


class Forbidden(Exception):
    pass


class MethodNotAllowed(Exception):
    pass


class Conflict(Exception):
    pass


class DeniedRequest(Exception):
    pass


class Unprocessable(Exception):
    pass


class ServerError(Exception):
    pass


class RequestFailed(Exception):
    pass
