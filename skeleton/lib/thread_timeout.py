from concurrent.futures import ThreadPoolExecutor, TimeoutError as FuturesTimeoutError


def thread_timeout(duration: int = 10):
    def wrapper(func):
        def func_decorator(*args, **kwargs):
            executor = ThreadPoolExecutor(max_workers=1)

            thr = executor.submit(func, *args, **kwargs)

            try:
                ret = thr.result(timeout=duration)
            except FuturesTimeoutError:
                raise TimeoutError(
                    "Timeout after running for {} s".format(duration))

            return ret

        return func_decorator

    return wrapper