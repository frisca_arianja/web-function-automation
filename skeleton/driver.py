from typing import TYPE_CHECKING, Union, Tuple, List
from selenium.webdriver import ChromeOptions, FirefoxOptions, Chrome, Firefox, Remote, DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from time import sleep

from .wait_element import WaitElement

if TYPE_CHECKING:
    from selenium.webdriver.remote.webdriver import WebDriver


class Driver:
    def __init__(self, driver: 'WebDriver'):
        self._driver = driver
        self.__version = driver.capabilities.get(
            'browserVersion', '') or driver.capabilities.get('version', '')
        self.__browser = driver.capabilities.get('browserName', '')

    def __getattr__(self, attr):
        return getattr(self._driver, attr)

    @property
    def version(self) -> str:
        return self.__version

    @property
    def browser(self) -> str:
        return self.__browser

    @property
    def tabs_total(self) -> int:
        return len(self._driver.window_handles)

    @property
    def url(self) -> str:
        return self._driver.current_url

    @property
    def tabs(self) -> List[str]:
        return self._driver.window_handles

    def switch_to_tab(self, index: int = 0):
        self._driver.switch_to.window(self.tabs[index])

    def switch_to_latest_tab(self):
        if self.browser == "chrome":
            self.switch_to_tab(1)
        else:
            self.switch_to_tab(-1)

    def switch_to_first_tab(self):
        self.switch_to_tab(0)

    def sleep_wait(self, timeout: int):
        sleep(timeout)

    def wait(self, timeout: int):
        return WaitElement(self.__driver, timeout)

    def close(self):
        self._driver.close()

    def quit(self):
        self._driver.quit()

    def back(self):
        self._driver.back()

    def forward(self):
        self._driver.forward()

    def current_url(self):
        return self._driver.current_url()

    def goto(self, url):
        self._driver.get(url)

    def save_screenshot(self,
                        filename: str,
                        folder: str = './result_images/',
                        format_file: str = 'jpg'):
        self._driver.save_screenshot(folder + filename + "." + format_file)


def create_driver(
    browser_name: str = "chrome",
    options: dict = {}
) -> Tuple[bool, Union['WebDriver', Driver, Exception]]:
    args = options.get("arguments", [])
    remote = options.get("remote", None)

    if browser_name.lower() == "chrome":
        chrome_caps = None

        if args:
            chrome_opts = ChromeOptions()
            for arg in args:
                chrome_opts.add_argument(arg)

            chrome_caps = chrome_opts.to_capabilities()

        if remote:
            if not chrome_caps:
                chrome_caps = DesiredCapabilities.CHROME
            driver = Remote(command_executor='http://{}/wd/hub'.format(remote),
                            desired_capabilities=chrome_caps)
        else:
            driver = Chrome(executable_path=ChromeDriverManager().install(),
                            desired_capabilities=chrome_caps)
    elif browser_name.lower() == "firefox":
        firefox_caps = None

        if args:
            firefox_opts = FirefoxOptions()
            for arg in args:
                firefox_opts.add_argument(arg)

            firefox_caps = firefox_opts.to_capabilities()

        if remote:
            if not firefox_caps:
                firefox_caps = DesiredCapabilities.FIREFOX
            driver = Remote(command_executor='http://{}/wd/hub'.format(remote),
                            desired_capabilities=firefox_caps)
        else:
            driver = Firefox(executable_path=GeckoDriverManager().install(),
                             desired_capabilities=firefox_caps)
    else:
        return False, Exception("Browser Not Supported")

    windows_size = options.get("window_size", "")

    if type(windows_size) is list:
        driver.set_window_size(*windows_size)

    if windows_size == "max":
        driver.maximize_window()

    return True, Driver(driver)
