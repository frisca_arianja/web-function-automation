from importlib import util
from typing import Union, Callable, Dict, List, Any
from copy import deepcopy

from . import config


def edit_message_for_log(err: Union[Exception, str],
                         *args) -> Union[Exception, str]:
    added_arg = ""

    for i in range(len(args)):
        added_arg = added_arg + args[i]

        if i != len(args) - 1:
            added_arg += " "

    err_cp = deepcopy(err)

    if isinstance(err, Exception):
        err_cp.args = (added_arg + ": " + err_cp.args[0], ) + err_cp.args[1:]
        if hasattr(err_cp, "msg"):
            err_cp.msg = added_arg + ": " + err_cp.msg
        err_cp.with_traceback(err.__traceback__)
    else:
        err_cp = added_arg + ": " + str(err_cp)

    return err_cp


def function_wrapper(func: Callable, *args):
    try:
        ret = func(*args)
        return ret
    except Exception as exc:
        return False, exc


def get_case_function(file_location: str, case_id: int):
    try:
        file_dir, class_name = file_location.split(".")
        file_dir += ".py"

        spec = util.spec_from_file_location(class_name, file_dir)
        module = util.module_from_spec(spec)

        spec.loader.exec_module(module)

        class_data = getattr(module, class_name)
        func_data = getattr(class_data, "test_C" + str(case_id))

        return True, class_data, func_data
    except BaseException as exc:
        return False, exc


def case_ids_generator(tests_data: List[Dict[str, Any]]):
    for test in tests_data:
        yield test["case_id"]