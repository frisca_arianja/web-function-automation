import time
import json
import os
import sys
import argparse

from concurrent.futures import ThreadPoolExecutor, as_completed
from getopt import getopt, GetoptError
from math import floor
from typing import List, Callable, Iterable, Dict, Any, Tuple, Union
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from . import config
from .driver import create_driver
from .runner_library import get_case_function, function_wrapper, edit_message_for_log, case_ids_generator
from .lib.logging import Logging
from .lib.gitlab_issue import GitlabIssue
from .lib.time_lib import convert_to_elapsed_time
from .lib.message_printing import error_print, CaseCollectPrint
from .lib.lib.exception.exception import exc_to_generator
from .lib.lib.testrail.testrail_client import TestrailClient


def run_case(
    case_func: List[Callable], timeout: int, used_browsers: Dict[str, Any]
) -> Tuple[bool, int, Dict[str, str or bool]] or Exception:
    browser_res = {}

    workers_num = len(used_browsers)

    executor = ThreadPoolExecutor(max_workers=workers_num)

    thrs = []
    for browser in used_browsers:
        browser_option = used_browsers.get(browser, {})

        stat, driver = create_driver(browser, browser_option)
        if not stat:
            browser_res[browser] = {
                "status": False,
                "run": False,
                "message": driver
            }
            continue

        thrs.append(
            executor.submit(function_wrapper, case_func[1],
                            case_func[0](driver), timeout))

    case_status = True
    total_time = 0

    for thr in as_completed(thrs):
        res = thr.result()

        total_time += floor(res[4])

        browser_res[res[2]] = {
            "status": res[0],
            "run": True,
            "message": res[1],
            "time": res[4],
            "version": res[3]
        }

        case_status = case_status and res[0]

    return case_status, total_time, browser_res


def run_some_cases(run_id: int, case_ids: Iterable[int], obj: Dict[str, any],
                   used_browsers: Dict[str, Any], options: Dict[str, any]):
    parallel_num = options.get("parallel", None)

    if parallel_num:
        thrs = []

        executor = ThreadPoolExecutor(max_workers=parallel_num)

        for case_id in case_ids:
            thrs.append(
                executor.submit(run_one_case, run_id, case_id, obj,
                                used_browsers, options))

        for thr in as_completed(thrs):
            thr.result()
    else:
        for case_id in case_ids:
            run_one_case(run_id, case_id, obj, used_browsers, options)


def run_cases_by_run(run_id: int, obj: Dict[str, Any],
                     used_browsers: Dict[str, Any], options: Dict[str, Any]):
    testrail_client = TestrailClient(config.TR_URL, config.TR_USERNAME,
                                     config.TR_PASSWORD)

    try:
        res_tests = testrail_client.get_tests(run_id)
    except Exception as exc:
        error_print(str(exc))
        return

    case_ids = case_ids_generator(res_tests)

    run_some_cases(run_id, case_ids, obj, used_browsers, options)


def run_one_case(run_id: int, case_id: int, obj: Dict[str, any],
                 used_browsers: Dict[str, Any], options: Dict[str, any]):
    logging = options.get("logging", False)

    write_log = None
    if logging:
        write_log = Logging(file_name="C{}.log".format(case_id))

    issuing = options.get("issuing", False)

    gitlab_issue = None
    if issuing:
        gitlab_issue = GitlabIssue(case_id)

    ccp = CaseCollectPrint(case_id, write_log)

    if "C" + str(case_id) in obj["unautomated"]:
        ccp.warning = "The case cannot be tested automatically"
        print(ccp)
        return

    try:
        url = str(obj["C" + str(case_id)])
    except KeyError:
        ccp.error = "Test function not found!"
        print(ccp)
        return

    func_ret = get_case_function(url, case_id)
    if not func_ret[0]:
        ccp.error = func_ret[1]
        print(ccp)
        return

    timeout_opts = options.get("timeout", None)

    try:
        case_stat, total_time, browser_ress = run_case(func_ret[1:],
                                                       timeout_opts,
                                                       used_browsers)
    except Exception as exc:
        ccp.error = exc
        print(ccp)
        return

    testrail_client = TestrailClient(config.TR_URL, config.TR_USERNAME,
                                     config.TR_PASSWORD)
    testrail_comment = ""

    # Send to testrail and create log
    for browser in browser_ress:
        browser_res = browser_ress[browser]

        if not browser_res['run']:
            ccp.debug = "{}:".format(browser)
            ccp.error = browser_res['message'], edit_message_for_log(
                browser_res['message'], browser)
            continue

        elapsed_time = convert_to_elapsed_time(
            floor(browser_res.get('time', 0)))

        browser_message = browser_res.get('message', '')
        browser_version = browser_res.get('version', '')

        ccp.debug = "{} {} ({}):".format(browser, browser_version,
                                         elapsed_time)

        if not browser_res['status']:
            message_log = edit_message_for_log(browser_message, browser,
                                               browser_version)

            ccp.error = browser_message, message_log

            if run_id is not None:
                testrail_comment += "------ {} {} ({}) ------\nError: {}\n\n".format(
                    browser, browser_version, elapsed_time,
                    str(browser_message))

            # Put the gitlab issue
            if gitlab_issue:
                gitlab_issue.create_issue(
                    message_log, "{} {}".format(browser, browser_version))
        else:
            success_message = "Success" if not browser_message else browser_message
            ccp.success = success_message, "{} {}: {}".format(
                browser, browser_version, success_message)

            if run_id is not None:
                tr_success_message = config.TR_SUCCESS_MSG if browser_message is None else browser_message

                testrail_comment += "------ {} {} ({}) ------\nSuccess: {}\n\n".format(
                    browser, browser_version, elapsed_time, tr_success_message)

    if run_id is not None:
        ccp.debug = "Testrail:"
        if case_stat:
            testrail_status = testrail_client.PASSED
        else:
            testrail_status = testrail_client.FAILED

        try:
            testrail_client.send_result_by_case(
                run_id, case_id, testrail_status, testrail_comment,
                convert_to_elapsed_time(total_time))
        except Exception as exc:
            ccp.error = exc, edit_message_for_log(exc, "Testrail")
            print(ccp)
            return

        ccp.success = "Success", "Testrail: {}".format("Success")

    print(ccp)


def extract_arguments(argv: str) -> Tuple[List[int], Union[int, None]]:
    run_id = None
    case_ids = []

    parser = argparse.ArgumentParser()

    parser.add_argument("-r", "--run-id", help="Run id to be run", type=int)
    parser.add_argument("-c",
                        "--case-id",
                        nargs="+",
                        help="List of case id to be run",
                        type=int)

    args = parser.parse_args(sys.argv[1:])

    if args.run_id:
        run_id = args.run_id
    if args.case_id:
        case_ids = args.case_id

    if not run_id and not case_ids:
        raise Exception("Bad Request")

    return case_ids, run_id


def choose_used_browsers(
        run_browsers: Union[List[str], Dict[str, Any]],
        options_browsers: Union[List[str], str]) -> Dict[str, Any]:
    chosen_browsers = {}

    if not run_browsers:
        run_browsers = ["chrome"]

    if type(run_browsers) is dict:
        available_browsers = run_browsers
    else:
        available_browsers = {}
        for brw in run_browsers:
            available_browsers[brw] = {}

    if options_browsers == "all":
        return available_browsers
    else:
        if type(options_browsers) is str:
            options_browsers = [options_browsers]

        for brw in options_browsers:
            try:
                opt = available_browsers[brw]
            except KeyError:
                raise Exception("Browser {} not found".format(brw))

            chosen_browsers[brw] = opt

    return chosen_browsers


def install_used_browsers(
        used_browsers: Union[Dict[str, Any], List[str]]) -> None:
    for browser in used_browsers:
        if browser.lower() == "chrome":
            ChromeDriverManager().install()
        elif browser.lower() == "firefox":
            GeckoDriverManager().install()
        else:
            raise Exception("Browser Not Supported")


def runner_main(argv: str) -> Tuple[bool, Any]:
    try:
        with open('run.json', 'r') as run_file:
            run_json = run_file.read()
            run_data = json.loads(run_json)
    except Exception as exc:
        return False, exc

    try:
        with open('options.json', 'r') as opts_file:
            opts_json = opts_file.read()
            opts = json.loads(opts_json)
    except Exception as exc:
        return False, exc

    case_ids, run_id = extract_arguments(argv)

    start_time = time.time()

    options_browsers = opts.get("browsers", [])
    if not options_browsers:
        return False, Exception(
            "Please write the browsers used in options.json")

    run_browsers = run_data.get("browsers", {})
    if not run_browsers:
        return False, Exception(
            "Please write the available browsers in run.json")

    try:
        used_browsers = choose_used_browsers(run_browsers, options_browsers)
    except Exception is exc:
        return False, exc

    try:
        install_used_browsers(used_browsers)
    except Exception as exc:
        return False, exc

    if case_ids:
        if len(case_ids) == 1:
            try:
                run_one_case(run_id, case_ids[0], run_data.get("cases", {}),
                             used_browsers, opts)
            except Exception as exc:
                return False, exc
        else:
            try:
                run_some_cases(run_id, case_ids, run_data.get("cases", {}),
                               used_browsers, opts)
            except Exception as exc:
                return False, exc
    else:
        run_cases_by_run(run_id, run_data.get("cases", {}), used_browsers,
                         opts)

    end_time = time.time()
    print('\nTotal running time: %ss\n' % (end_time - start_time))

    return True, None
