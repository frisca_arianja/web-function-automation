import selenium.webdriver.support.ui as ui
import selenium.webdriver.support.expected_conditions as ec

from selenium.webdriver.common.by import By
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from selenium.webdriver.remote.webdriver import WebDriver


class WaitElement:
    def __init__(self, driver: 'WebDriver', timeout: int = 10):
        self.__timeout = timeout
        self.__driver = driver

    @property
    def timeout(self) -> int:
        return self.__timeout

    @property
    def driver(self) -> 'WebDriver':
        return self.__driver

    def is_visible(self, by: str, locator: str):
        ui.WebDriverWait(self.driver, self.timeout).until(
            ec.visibility_of_element_located((by, locator)))
