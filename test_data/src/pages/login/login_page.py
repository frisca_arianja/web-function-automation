import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from skeleton.base_page import BasePage
from selenium.webdriver.common.by import By

class LoginPage(BasePage):

    login_menu_xpath = '//*[@id="login"]/ul/li[1]/a'
    email_placeholder_id = 'email-field'
    password_placeholder_id = 'password-field'
    password_icon_xpath = '//*[@id="form-password"]/div/span/i'
    login_btn_xpath = '//*[@id="loginForm"]/a[1]'
    error_email_placeholder_xpath = '//*[@id="form-email"]/div/div'
    error_password_placeholder_xpath = '//*[@id="form-password"]/div/div'
    username_dropdown_xpath = '//*[@id="drop2"]/span/span'
    emsg_xpath = '//*[@id="errorPanel"]/div/div'

    vars = {}

    def go_to_login_page(self):
        self._driver.find_element(
            By.XPATH, LoginPage.login_menu_xpath).is_displayed()
        title = self._driver.find_element(
            By.XPATH, LoginPage.login_menu_xpath).text
        self._driver.find_element(
            By.XPATH, LoginPage.login_menu_xpath).click()
        return title

    def input_email(self, email: str):
        self._driver.find_element(
            By.ID, LoginPage.email_placeholder_id).send_keys(email)

    def check_email_value(self):
        email_value = self._driver.find_element(
            By.ID, LoginPage.email_placeholder_id).get_attribute('value')
        return email_value
        
    def input_password(self, password: str):
        self._driver.find_element(
            By.ID, LoginPage.password_placeholder_id).send_keys(password)

    def click_password_icon(self):
        self._driver.find_element(
            By.XPATH, LoginPage.password_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, LoginPage.password_icon_xpath).click()
        
    def check_password_value(self):
        password_value = self._driver.find_element(
            By.ID, LoginPage.password_placeholder_id).get_attribute('value')
        return password_value
        
    def click_login_button(self):
        self._driver.find_element(
            By.XPATH, LoginPage.login_btn_xpath).click()

    def check_error_msg_email(self): 
        error_email = self._driver.find_element(
            By.XPATH, LoginPage.error_email_placeholder_xpath).text 
        return error_email

    def check_error_msg_password(self):
        error_password = self._driver.find_element(
            By.XPATH, LoginPage.error_password_placeholder_xpath).text 
        return error_password

    def username_text(self):
        username = self._driver.find_element(
            By.XPATH, LoginPage.username_dropdown_xpath).text
        return username

    def check_emsg_value(self): 
        msg_value = self._driver.find_element(
            By.XPATH, LoginPage.emsg_xpath).text 
        return msg_value