import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from skeleton.base_page import BasePage

class DashboardPage(BasePage):

    plane_menu_xpath = '//*[@id="index-page"]/body/div[1]/div[2]/div[2]/div[1]/ul/li[2]/a'
    departure_city_id = 'asalKota1'
    dropdown_departure_city_id = 'ui-id-1'

    destination_city_id = 'tujuanKota1'
    dropdown_destination_city_id = 'ui-id-2'

    next_btn_xpath = '//*[@id="ui-datepicker-div"]/div[2]/div/a'
    date1_xpath = '//*[@id="ui-datepicker-div"]/div[1]/table/tbody/tr[1]/td[2]/a'
    date2_xpath = '//*[@id="ui-datepicker-div"]/div[1]/table/tbody/tr[4]/td[1]/a'

    date_go_id = 'tglKeberangkatan'
    date_back_id = 'tglPulang'

    adults_id = 'dewasa'
    children_id = 'anak'
    baby_id = 'balita'

    num_of_adults_xpath = '//*[@id="dewasa"]/option[2]'
    num_of_children_xpath = '//*[@id="anak"]/option[2]'
    num_of_baby_xpath = '//*[@id="balita"]/option[2]'

    btn_find_tickets_id = 'cari_tiket'

    city_xpath = '/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]'
    date_and_passanger_xpath = '/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]'

    plane_type_1 = '//*[@id="flightDetailDiv"]/div/div[1]/div[2]'
    plane_type_2 = '//*[@id="flightPulangDetailDiv"]/div[1]/div[1]/div[2]'

    date_check_p1 = '//*[@id="flightDetailDiv"]/div/div[1]/div[3]'
    date_check_p2 = '//*[@id="flightPulangDetailDiv"]/div[1]/div[1]/div[3]'

    title_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/select'
    title_option = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/select/option[2]'

    firstname_data = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/input'
    lastname_data = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/input'
    phone_number_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[5]/div[2]/input'
    email_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[6]/div[2]/input'

    salutation_option_1 = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/select/option[2]'
    salutation_option_2 = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/select/option[1]'
    salutation_option_3 = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[1]/select/option[2]'
    salutation_option_4 = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[1]/div[1]/select/option[1]'

    c_salutation_1 = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[1]/span/table/tbody/tr[1]/td[2]'
    c_salutation_2 = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[2]/span/table/tbody/tr[1]/td[2]'
    c_salutation_3 = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[3]/span/table/tbody/tr[1]/td[2]'
    c_salutation_4 = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[4]/span/table/tbody/tr[1]/td[2]'

    c_name_1 = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[1]/span/table/tbody/tr[2]/td[1]'
    c_name_2 = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[2]/span/table/tbody/tr[2]/td[1]'
    c_child = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[3]/span/table/tbody/tr[2]/td[1]'
    c_baby = '/html/body/div[1]/div[2]/div[2]/div/div[6]/div/ul/li[4]/span/table/tbody/tr[2]/td[1]'

    add_luggage_back = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[5]/div[1]/div/div[3]/div[1]/select/option[2]'
    add_luggage_go = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[4]/div[1]/div/div[2]/div/select/option[2]'

    firstname_name = 'namaDepandewasa[]'
    lastname_name = 'namaBelakangdewasa[]'

    firstname_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div/input'
    lastname_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[3]/div/input'
    firstname_child = 'namaDepananak[]'
    lastname_child = 'namaBelakanganak[]'

    date_component_name = 'tgl_lahiranak[]'
    date_child = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[2]/div[1]/select/option[9]'
    month_component_name = 'bln_lahiranak[]'
    month_child = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[2]/div[2]/select/option[13]'
    year_component_name = 'thn_lahiranak[]'
    year_child = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[3]/div/select/option[8]'

    invalid_date_child = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/div[1]/select/option[32]'
    invalid_month_child = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[2]/div[2]/select/option[13]'
    invalid_year_child = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[3]/div/select/option[2]'

    firstname_baby = 'namaDepanbayi[]'
    lastname_baby = 'namaBelakangbayi[]'

    date_baby_component_name = 'tgl_lahirbayi[]'
    date_baby = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/div[1]/select/option[9]'
    month_baby_component_name = 'bln_lahirbayi[]'
    month_baby = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/div[2]/select/option[4]'
    year_baby_component_name = 'thn_lahirbayi[]'
    year_baby = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[3]/div/select/option[3]'

    invalid_date_baby = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/div[1]/select/option[32]'
    invalid_month_baby = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/div[2]/select/option[9]'
    invalid_year_baby = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[3]/div/select/option[2]'

    slider_toogle_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[5]/div[2]/div[1]/div/div[2]/label/span'
    rec_name_xpath = '/html/body/div[1]/div[2]/div[1]/div[3]/div/div[2]/div[5]/div[1]/div[3]/div[1]/input'
    order_id = '/html/body/div[1]/div[2]/div[2]/div/div[1]/span[2]'

    e_firstname_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div/span'
    e_lastname_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[3]/div/span'
    e_firstname2_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div/span'
    e_lastname2_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[3]/div/span'
    e_firstname_adult_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[2]/div/span'
    e_lastname_adult_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[3]/div/span'
    e_firstname_baby_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[1]/div[2]/div/span'
    e_lastname_baby_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[1]/div[3]/div/span'
    e_date_format_adult_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[2]/span'
    e_date_format_baby_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/span'

    e_child_age_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div[1]/div[2]/span[2]'
    e_baby_age_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div[1]/div[2]/span[2]'

    e_modal_msg_id = 'errorMessageModal'
    insurance_Link_id = 'insuranceLink'

    e_term_xpath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[6]/div[1]/div/span'

    def go_to_tiket_pesawat_page(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.plane_menu_xpath).is_displayed()
        title = self._driver.find_element(
            By.XPATH, DashboardPage.plane_menu_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.plane_menu_xpath).click()
        return title

    def input_departure_city(self, kota_asal: str):
        self._driver.find_element(
            By.ID, DashboardPage.departure_city_id).clear()
        self._driver.find_element(
            By.ID, DashboardPage.departure_city_id).send_keys(kota_asal)
        time.sleep(1)
        self._driver.find_element(
            By.ID, DashboardPage.dropdown_departure_city_id).click()

    def input_destination_city(self, kota_tujuan: str):
        self._driver.find_element(
            By.ID, DashboardPage.destination_city_id).clear()
        self._driver.find_element(
            By.ID, DashboardPage.destination_city_id).send_keys(kota_tujuan)
        time.sleep(1)
        self._driver.find_element(
            By.ID, DashboardPage.dropdown_destination_city_id).click()

    def select_date_go(self):
        self._driver.find_element(
            By.ID, DashboardPage.date_go_id).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.next_btn_xpath).click()
        time.sleep(1)
        self._driver.find_element(
            By.XPATH, DashboardPage.date1_xpath).click()

    def select_date_back(self):
        self._driver.find_element(
            By.ID, DashboardPage.date_back_id).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.date2_xpath).click()

    def select_nom_adults(self):
        self._driver.find_element(
            By.ID, DashboardPage.adults_id).click()
        time.sleep(1)
        self._driver.find_element(
            By.XPATH, DashboardPage.num_of_adults_xpath).click()

    def select_nom_children(self):
        self._driver.find_element(
            By.ID, DashboardPage.children_id).click()
        time.sleep(1)
        self._driver.find_element(
            By.XPATH, DashboardPage.num_of_children_xpath).click() 

    def select_nom_baby(self):
        self._driver.find_element(
            By.ID, DashboardPage.baby_id).click()
        time.sleep(1)
        self._driver.find_element(
            By.XPATH, DashboardPage.num_of_baby_xpath).click() 

    def find_tickets(self):
        self._driver.find_element(
            By.ID, DashboardPage.btn_find_tickets_id).click() 
        
    def check_city(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.city_xpath).is_displayed()
        city_value = self._driver.find_element(
            By.XPATH, DashboardPage.city_xpath).text
        return city_value

    def check_date_and_passanger(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.date_and_passanger_xpath).is_displayed()
        passanger_value = self._driver.find_element(
            By.XPATH, DashboardPage.date_and_passanger_xpath).text
        return passanger_value

    def click_order(self): 
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=submit]").click()

    def check_plane_name(self):
        plane_name1 = self._driver.find_element(
            By.XPATH, DashboardPage.plane_type_1).text
        plane_name2 = self._driver.find_element(
            By.XPATH, DashboardPage.plane_type_2).text
        return plane_name1, plane_name2 
        
    def check_date_of_plane(self):
        date_plane1 = self._driver.find_element(
            By.XPATH, DashboardPage.date_check_p1).text
        date_plane_2 = self._driver.find_element(
            By.XPATH, DashboardPage.date_check_p2).text
        return date_plane1,date_plane_2

    def choose_title(self): 
        self._driver.find_element(
            By.XPATH, DashboardPage.title_xpath).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.title_option).click()

    def input_phone_number(self, phone_number: str):
        self._driver.find_element(
            By.XPATH, DashboardPage.phone_number_xpath).send_keys(phone_number)

    def input_firstname_adult1(self, firstname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.salutation_option_1).click()
        self._driver.find_element(
            By.NAME, DashboardPage.firstname_name).send_keys(firstname)

    def input_lastname_adult1(self, lastname: str): 
        self._driver.find_element(
            By.NAME, DashboardPage.lastname_name).send_keys(lastname)

    def input_firstname_adult2(self, firstname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.salutation_option_2).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.firstname_xpath).send_keys(firstname)

    def input_lastname_adult2(self, lastname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.lastname_xpath).send_keys(lastname)
        
    def add_luggage(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.add_luggage_go).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.add_luggage_back).click()
    
    def input_firstname_child(self, firstname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.salutation_option_3).click()
        self._driver.find_element(
            By.NAME, DashboardPage.firstname_child).send_keys(firstname)

    def input_lastname_child(self, lastname: str): 
        self._driver.find_element(
            By.NAME, DashboardPage.lastname_child).send_keys(lastname) 
        
    def date_child_born(self):
        self._driver.find_element(
            By.NAME, DashboardPage.date_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.date_child).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.month_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.month_child).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.year_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.year_child).click() 

    def invalid_date_child_born(self):
        self._driver.find_element(
            By.NAME, DashboardPage.date_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.invalid_date_child).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.month_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.invalid_month_child).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.year_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.invalid_year_child).click() 

    def input_firstname_baby(self, firstname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.salutation_option_4).click()
        self._driver.find_element(
            By.NAME, DashboardPage.firstname_baby).send_keys(firstname)

    def input_lastname_baby(self, lastname: str): 
        self._driver.find_element(
            By.NAME, DashboardPage.lastname_baby).send_keys(lastname) 

    def date_baby_born(self):
        self._driver.find_element(
            By.NAME, DashboardPage.date_baby_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.date_baby).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.month_baby_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.month_baby).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.year_baby_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.year_baby).click() 

    def invalid_date_baby_born(self):
        self._driver.find_element(
            By.NAME, DashboardPage.date_baby_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.invalid_date_baby).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.month_baby_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.invalid_month_baby).click() 
        self._driver.find_element(
            By.NAME, DashboardPage.year_baby_component_name).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.invalid_year_baby).click() 

    def click_toogle(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.slider_toogle_xpath).click()

    def click_next(self): 
        self._driver.find_element(
            By.CSS_SELECTOR, "[value=Lanjut]").click()

    def check_all_passangers_data(self):
        sal_1 = self._driver.find_element(
            By.XPATH, DashboardPage.c_salutation_1).text
        print(sal_1)
        sal_2 = self._driver.find_element(
            By.XPATH, DashboardPage.c_salutation_2).text
        print(sal_2)
        sal_3 = self._driver.find_element(
            By.XPATH, DashboardPage.c_salutation_3).text
        print(sal_3)
        sal_4 = self._driver.find_element(
            By.XPATH, DashboardPage.c_salutation_4).text
        print(sal_4)
        name_1 = self._driver.find_element(
            By.XPATH, DashboardPage.c_name_1).text
        print(name_1)
        name_2 = self._driver.find_element(
            By.XPATH, DashboardPage.c_name_2).text
        print(name_2)
        child_nm = self._driver.find_element(
            By.XPATH, DashboardPage.c_child).text
        print(child_nm)
        baby_nm = self._driver.find_element(
            By.XPATH, DashboardPage.c_baby).text
        print(baby_nm)
        return sal_1,sal_2,sal_3,sal_4,name_1,name_2,child_nm,baby_nm

    def print_order_id(self): 
        number_of_order = self._driver.find_element(
            By.CSS_SELECTOR, "[class=number]").text
        print(number_of_order)

    def error_phone_number(self): 
        error = self._driver.find_element(
            By.CSS_SELECTOR, "[class=errorTelp]").text
        return error
    
    def error_firstname(self): 
        error = self._driver.find_element(
            By.CSS_SELECTOR, "[class=errorNamaDepan]").text
        return error

    def error_lastname(self): 
        error = self._driver.find_element(
            By.CSS_SELECTOR, "[class=errorNamaBelakang]").text
        return error

    def error_email(self): 
        error = self._driver.find_element(
            By.CSS_SELECTOR, "[class=errorEmail]").text
        return error

    def clear_owner_account_data(self): 
        self._driver.find_element(
            By.NAME, DashboardPage.firstname_name).clear()
        self._driver.find_element(
            By.NAME, DashboardPage.lastname_name).clear()

    def error_invalid_phone(self): 
        invalid_phone = self._driver.find_element(
            By.ID, DashboardPage.e_modal_msg_id).text
        return invalid_phone

    def check_all_error_name_passengers(self): 
        e_firstname_1 = self._driver.find_element(
            By.XPATH, DashboardPage.e_firstname_xpath).text
        e_lastname_1 = self._driver.find_element(
            By.XPATH, DashboardPage.e_lastname_xpath).text
        e_firstname_2 = self._driver.find_element(
            By.XPATH, DashboardPage.e_firstname2_xpath).text
        e_lastname_2 = self._driver.find_element(
            By.XPATH, DashboardPage.e_lastname2_xpath).text
        e_firstname_adult = self._driver.find_element(
            By.XPATH, DashboardPage.e_firstname_adult_xpath).text
        e_lastname_adult = self._driver.find_element(
            By.XPATH, DashboardPage.e_lastname_adult_xpath).text
        e_firstname_baby = self._driver.find_element(
            By.XPATH, DashboardPage.e_firstname_baby_xpath).text
        e_lastname_baby = self._driver.find_element(
            By.XPATH, DashboardPage.e_lastname_baby_xpath).text
        e_date_adult = self._driver.find_element(
            By.XPATH, DashboardPage.e_date_format_adult_xpath).text
        e_date_baby = self._driver.find_element(
            By.XPATH, DashboardPage.e_date_format_baby_xpath).text
        return e_firstname_1, e_lastname_1, e_firstname_2, e_lastname_2, e_firstname_adult, e_lastname_adult, e_firstname_baby, e_lastname_baby, e_date_adult, e_date_baby

    def error_minimum_age(self): 
        child_age = self._driver.find_element(
            By.XPATH, DashboardPage.e_child_age_xpath).text
        baby_age = self._driver.find_element(
            By.XPATH, DashboardPage.e_baby_age_xpath).text
        return child_age, baby_age

    def clear_rec_name(self): 
        self._driver.find_element(
            By.XPATH, DashboardPage.rec_name_xpath).click()
        self._driver.find_element(
            By.XPATH, DashboardPage.rec_name_xpath).clear()

    def check_link(self): 
        self._driver.find_element(
            By.ID, DashboardPage.insurance_Link_id).click()

    def click_next_btn(self): 
        self._driver.find_element(
            By.CSS_SELECTOR, "[value=Lanjutkan]").click()

    def input_firstname(self, firstname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.firstname_data).send_keys(firstname) 

    def input_lastname(self, lastname: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.lastname_data).send_keys(lastname) 

    def input_email(self, email: str): 
        self._driver.find_element(
            By.XPATH, DashboardPage.email_xpath).send_keys(email)    

    def check_error_term(self): 
        error = self._driver.find_element(
            By.XPATH, DashboardPage.e_term_xpath).text
        return error