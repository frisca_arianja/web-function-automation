from test_data.src.pages.dashboard.dashboard_page import DashboardPage
from test_data.src.pages.login.login_page import LoginPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from test_data.test_cases.login_test.login_test import Login
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time

class Dashboard(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

# Find tickets and get order_id 
    @TestTemplate.test_func()
    def test_C1734(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.choose_title()
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.click_order()

            dashboard_page.clear_owner_account_data()
            dashboard_page.input_firstname_adult1(const.FIRSTNAME_1)
            dashboard_page.input_lastname_adult1(const.LASTNAME_1)
            dashboard_page.add_luggage()
            dashboard_page.input_firstname_adult2(const.FIRSTNAME_2)
            dashboard_page.input_lastname_adult2(const.LASTNAME_2)
            dashboard_page.input_firstname_child(const.FIRSTNAME_CHILD)
            dashboard_page.input_lastname_child(const.LASTNAME_2)
            dashboard_page.date_child_born()
            dashboard_page.input_firstname_baby(const.FIRSTNAME_BABY)
            dashboard_page.input_lastname_baby(const.LASTNAME_2)
            dashboard_page.date_baby_born()
            dashboard_page.click_toogle()
            dashboard_page.click_order()
            time.sleep(10)

            dashboard_page.click_next()
            salutation_1, salutation_2, salutation_3, salutation_4, name_1, name2, child_name, baby_name = dashboard_page.check_all_passangers_data()
            assert salutation_1 == const.SALUTATION_1, "salutation first person not match"
            assert salutation_2 == const.SALUTATION_2, "salutation second person not match"
            assert salutation_3 == const.SALUTATION_3, "salutation the adult person not match"
            assert salutation_4 == const.SALUTATION_2, "salutation the baby person not match"
            assert name_1 == const.FIRSTNAME_1+" "+const.LASTNAME_1, "name of first person not accordance"
            assert name2 == const.FIRSTNAME_2+" "+const.LASTNAME_2, "name of second person not accordance"
            assert child_name == const.FIRSTNAME_CHILD+" "+const.LASTNAME_2, "name of the child not accordance"
            assert baby_name == const.FIRSTNAME_BABY+" "+const.LASTNAME_2, "name of the baby not accordance"

            dashboard_page.print_order_id()
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

 # Leave Blank Phone Number
    @TestTemplate.test_func()
    def test_C6812(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.click_order()
            e_msg = dashboard_page.error_phone_number()
            assert e_msg == const.ERROR_LEAVE_PHONE, "error message for leave blank phone number not accordance"
            time.sleep(3)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Wrong Format Phone Number
    @TestTemplate.test_func()
    def test_C6651(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.input_phone_number(const.WRONG_FORMAT_PHONE_NUMBER)
            e_msg = dashboard_page.error_phone_number()
            assert e_msg == const.ERROR_WRONG_PHONE, "error message wrong format phone number not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Invalid Phone number
    @TestTemplate.test_func()
    def test_C6944(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.choose_title()
            dashboard_page.input_phone_number(const.INVALID_PHONE_NUMBER)
            dashboard_page.click_order()

            dashboard_page.clear_owner_account_data()
            dashboard_page.input_firstname_adult1(const.FIRSTNAME_1)
            dashboard_page.input_lastname_adult1(const.LASTNAME_1)
            dashboard_page.input_firstname_adult2(const.FIRSTNAME_2)
            dashboard_page.input_lastname_adult2(const.LASTNAME_2)
            dashboard_page.input_firstname_child(const.FIRSTNAME_CHILD)
            dashboard_page.input_lastname_child(const.LASTNAME_2)
            dashboard_page.date_child_born()
            dashboard_page.input_firstname_baby(const.FIRSTNAME_BABY)
            dashboard_page.input_lastname_baby(const.LASTNAME_2)
            dashboard_page.date_baby_born()
            dashboard_page.click_toogle()
            dashboard_page.click_order()
            time.sleep(5)

            e_invalid_phone = dashboard_page.error_invalid_phone()
            assert e_invalid_phone == const.ERROR_INVALID_PHONE, "error message invalid phone number not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Leave blank all passenger data
    @TestTemplate.test_func()
    def test_C6927(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.choose_title()
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.click_order()

            dashboard_page.clear_owner_account_data()
            dashboard_page.click_order()
            time.sleep(2)

            firstname_1, lastname_1, firstname_2, lastname_2, firstname_adult, lastname_adult, firstname_baby, lastname_baby, date_child, date_baby = dashboard_page.check_all_error_name_passengers()
            assert firstname_1 == const.ERROR_BLANK_NAME, "error message leave blank firstname passanger 1 not accordance"
            assert lastname_1 == const.ERROR_BLANK_NAME, "error message leave blank lastname passanger 1 not accordance"

            assert firstname_2 == const.ERROR_BLANK_NAME, "error message leave blank firstname passanger 2 not accordance"
            assert lastname_2 == const.ERROR_BLANK_NAME, "error message leave blank lastname passanger 2 not accordance"

            assert firstname_adult == const.ERROR_BLANK_NAME, "error message leave blank firstname adult passanger not accordance"
            assert lastname_adult == const.ERROR_BLANK_NAME, "error message leave blank lastname adult passanger not accordance"

            assert firstname_baby == const.ERROR_BLANK_NAME, "error message leave blank firstname baby passanger not accordance"
            assert lastname_baby == const.ERROR_BLANK_NAME, "error message leave blank lastname baby passanger not accordance"
            assert date_child == const.ERROR_BLANK_DATE, "error message leave blank date child passanger not accordance"
            assert date_baby == const.ERROR_BLANK_DATE, "error message leave blank date baby passanger not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#the age of the child is not appropriate
    @TestTemplate.test_func()
    def test_C6929(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.choose_title()
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.click_order()

            dashboard_page.clear_owner_account_data()
            dashboard_page.input_firstname_adult1(const.FIRSTNAME_1)
            dashboard_page.input_lastname_adult1(const.LASTNAME_1)
            dashboard_page.add_luggage()
            dashboard_page.input_firstname_adult2(const.FIRSTNAME_2)
            dashboard_page.input_lastname_adult2(const.LASTNAME_2)
            dashboard_page.input_firstname_child(const.FIRSTNAME_CHILD)
            dashboard_page.input_lastname_child(const.LASTNAME_2)
            dashboard_page.invalid_date_child_born()
            dashboard_page.input_firstname_baby(const.FIRSTNAME_BABY)
            dashboard_page.input_lastname_baby(const.LASTNAME_2)
            dashboard_page.invalid_date_baby_born()
            dashboard_page.click_toogle()
            dashboard_page.click_order()

            error_child, error_baby = dashboard_page.error_minimum_age()
            assert error_child == const.ERROR_CHILD_AGE, "error message of min child age not accordance"
            assert error_baby == const.ERROR_BABY_AGE, "error message of min baby age not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# verify insurance link work
    @TestTemplate.test_func()
    def test_C1653(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.choose_title()
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.click_order()

            dashboard_page.clear_owner_account_data()
            dashboard_page.input_firstname_adult1(const.FIRSTNAME_1)
            dashboard_page.input_lastname_adult1(const.LASTNAME_1)
            dashboard_page.add_luggage()
            dashboard_page.input_firstname_adult2(const.FIRSTNAME_2)
            dashboard_page.input_lastname_adult2(const.LASTNAME_2)
            dashboard_page.input_firstname_child(const.FIRSTNAME_CHILD)
            dashboard_page.input_lastname_child(const.LASTNAME_2)
            dashboard_page.date_child_born()
            dashboard_page.input_firstname_baby(const.FIRSTNAME_BABY)
            dashboard_page.input_lastname_baby(const.LASTNAME_2)
            dashboard_page.date_baby_born()
            dashboard_page.check_link()
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# check error message when leave blank all contact data
    @TestTemplate.test_func()
    def test_C1654(self):
        try:
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()

            dashboard_page.click_next_btn()

            e_firstname = dashboard_page.error_firstname()
            assert e_firstname == const.ERROR_FIRSTNAME, "error message for leave blank firstname not accordance"

            e_lastname = dashboard_page.error_lastname()
            assert e_lastname == const.ERROR_LASTNAME, "error message for leave blank lastname not accordance"

            e_phone_num = dashboard_page.error_phone_number()
            assert e_phone_num == const.ERROR_LEAVE_PHONE, "error message for leave blank phone number not accordance"

            e_email = dashboard_page.error_email()
            assert e_email == const.ERROR_EMAIL, "error message for leave blank email not accordance"
            
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# wrong format email
    @TestTemplate.test_func()
    def test_C1655(self):
        try:
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.input_firstname(const.FIRSTNAME_1)
            dashboard_page.input_lastname(const.LASTNAME_1)
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.input_email(const.WRONG_FORMAT_EMAIL)

            e_email = dashboard_page.error_email()
            assert e_email == const.ERROR_EMAIL, "error message for leave blank email not accordance"
            time.sleep(1)
            
            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# not click F&Q checkbox
    @TestTemplate.test_func()
    def test_C1656(self):
        try:
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.input_firstname(const.FIRSTNAME_1)
            dashboard_page.input_lastname(const.LASTNAME_1)
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.input_email(const.EMAIL)
            dashboard_page.click_next_btn()

            dashboard_page.clear_owner_account_data()
            dashboard_page.input_firstname_adult1(const.FIRSTNAME_1)
            dashboard_page.input_lastname_adult1(const.LASTNAME_1)
            dashboard_page.add_luggage()
            dashboard_page.input_firstname_adult2(const.FIRSTNAME_2)
            dashboard_page.input_lastname_adult2(const.LASTNAME_2)
            dashboard_page.input_firstname_child(const.FIRSTNAME_CHILD)
            dashboard_page.input_lastname_child(const.LASTNAME_2)
            dashboard_page.date_child_born()
            dashboard_page.input_firstname_baby(const.FIRSTNAME_BABY)
            dashboard_page.input_lastname_baby(const.LASTNAME_2)
            dashboard_page.date_baby_born()
            dashboard_page.click_toogle()
            dashboard_page.click_order()
            time.sleep(1)
            error_term = dashboard_page.check_error_term()
            assert error_term == const.ERROR_TERM, "error message not match"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Leave Blank rec_name
    @TestTemplate.test_func()
    def test_C6928(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            menu = dashboard_page.go_to_tiket_pesawat_page()
            assert menu == const.TITLE_MENU, "Title not accordance"
            time.sleep(1)

            dashboard_page.input_departure_city(const.Departure)
            dashboard_page.input_destination_city(const.Destination)

            dashboard_page.select_date_go()
            dashboard_page.select_date_back()

            dashboard_page.select_nom_adults()
            dashboard_page.select_nom_children()
            dashboard_page.select_nom_baby()
            dashboard_page.find_tickets()
            time.sleep(2)

            city_value = dashboard_page.check_city()
            assert city_value == const.Departure+" ke "+const.Destination, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Senin, 01 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 
            time.sleep(4) 

            city_value = dashboard_page.check_city()
            assert city_value == const.Destination+" ke "+const.Departure, "City name not accordance" 

            passanger_value = dashboard_page.check_date_and_passanger()
            assert passanger_value == "Minggu, 21 Juni 2020 | 2 Dewasa, 1 Anak, 1 Bayi", "date value or total passanger not accordance"
            dashboard_page.click_order() 

            plane_name1, plane_name2 = dashboard_page.check_plane_name()
            assert plane_name1 == const.NUMBER_PLANE_1, "name and type of plane is wrong"
            assert plane_name2 == const.NUMBER_PLANE_2, "name and type of plane is wrong"

            date_plane_1, date_plane_2 = dashboard_page.check_date_of_plane()
            assert date_plane_1 == "(Senin, 1 Juni 2020)", "date of departure not accordance"
            assert date_plane_2 == "(Minggu, 21 Juni 2020)", "date of departure not accordance"
            dashboard_page.click_order()
            time.sleep(1)

            dashboard_page.choose_title()
            dashboard_page.input_phone_number(const.PHONE_NUMBER)
            dashboard_page.click_order()

            dashboard_page.clear_owner_account_data()
            dashboard_page.input_firstname_adult1(const.FIRSTNAME_1)
            dashboard_page.input_lastname_adult1(const.LASTNAME_1)
            dashboard_page.add_luggage()
            dashboard_page.input_firstname_adult2(const.FIRSTNAME_2)
            dashboard_page.input_lastname_adult2(const.LASTNAME_2)
            dashboard_page.input_firstname_child(const.FIRSTNAME_CHILD)
            dashboard_page.input_lastname_child(const.LASTNAME_2)
            dashboard_page.date_child_born()
            dashboard_page.input_firstname_baby(const.FIRSTNAME_BABY)
            dashboard_page.input_lastname_baby(const.LASTNAME_2)
            dashboard_page.date_baby_born()
            dashboard_page.click_toogle()
            dashboard_page.click_order()
            time.sleep(10)
            dashboard_page.click_next()
            dashboard_page.clear_rec_name()
            dashboard_page.click_order()
            time.sleep(5)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc
