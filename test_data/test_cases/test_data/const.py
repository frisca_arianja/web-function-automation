from os import getcwd

DOMAIN = "https://www.pegipegi.com/"

#Data Login User
EMAIL = "qacloud12@gmail.com"
PASSWORD = "Arianja*8"
NAME = "frisca"

LOGIN_TITLE = "Login"
ERROR_MSG_EMAIL = "Email harus diisi dengan format yang benar (Contoh: email@example.com)"
ERROR_MSG_PASSWORD = "Password harus diisi (6-20 karakter, dapat berupa angka dan/atau huruf)"
MSG_NOT_VERIFIED_ACCNT = "Akun Anda belum aktif, silahkan cek email Anda untuk aktivasi atau kirim ulang aktivasi email"
ERROR_WRONG_PASSWORD = "Email atau kata kunci salah"
ERROR_BLANK_PASSWORD = "Field password tidak boleh kosong"
MSG_EMAIL_NOT_REGISTERED = "Email tidak terdaftar"
MSG_MIN_PASSWORD = "Field password minimum 6 karakter"
ERROR_TERM = "Data ini dibutuhkan"

UNVERIFIED_EMAIL = "qacloud13@gmail.com" 
WRONG_PASSWORD = "Arianja88"
EMAIL_NOT_REGRIS = "frisca@nodeflux.io"
WRONG_FORMAT_PASSWORD = 'Frisa'

TITLE_MENU = "Pesawat"
Destination = "Kendari"
Departure = "Surabaya"

NUMBER_PLANE_1 = "Lion Air JT 992"
NUMBER_PLANE_2 = "Lion Air JT 727"

PHONE_NUMBER = "085943511582"
WRONG_FORMAT_PHONE_NUMBER = "082299"
INVALID_PHONE_NUMBER = '86543567'

WRONG_FORMAT_EMAIL = "qacloud12@gmailcom"

FIRSTNAME_1 = 'Frisca'
LASTNAME_1 = 'Arianja'

FIRSTNAME_2 = 'Freddy'
LASTNAME_2 = 'Wijaya'

FIRSTNAME_CHILD = 'Sisca Franda'
FIRSTNAME_BABY = 'Michael Jonathan'

SALUTATION_1 = 'Nyonya'
SALUTATION_2 = 'Tuan'
SALUTATION_3 = 'Nona'

INSURANCE_PAGE = 'Asuransi Penerbangan'

ERROR_LEAVE_PHONE = 'No Telepon belum benar'
ERROR_WRONG_PHONE = 'Nomor Telepon tidak valid (minimal 7 karakter)'
ERROR_INVALID_PHONE = 'Nomor Anda tidak valid. Mohon periksa kembali nomor Anda.'

ERROR_BLANK_NAME = "Data ini dibutuhkan"
ERROR_BLANK_DATE = "Format tanggal tidak tepat"
ERROR_CHILD_AGE = 'Usia anak harus berumur 2-11 tahun pada tanggal penerbangan 2020-06-21.'
ERROR_BABY_AGE = 'Usia bayi harus kurang dari 2 tahun pada tanggal penerbangan 2020-06-21.'

ERROR_FIRSTNAME = 'Nama Depan belum benar'
ERROR_LASTNAME = 'Nama Belakang belum benar'
ERROR_EMAIL = 'Email belum benar'