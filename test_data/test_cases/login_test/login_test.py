from test_data.src.pages.dashboard.dashboard_page import DashboardPage
from test_data.src.pages.login.login_page import LoginPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
import time

class Login(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

#Leave all field blank
    @TestTemplate.test_func()
    def test_C3479(self):
        try:
            login_page = LoginPage(self._driver)

            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title not accordance"
            time.sleep(1)
            login_page.input_email("")
            login_page.input_password("")
            login_page.click_login_button()

            error_email = login_page.check_error_msg_email()
            assert error_email == const.ERROR_MSG_EMAIL, "email error message not accordance"
            error_password = login_page.check_error_msg_password()
            assert error_password == const.ERROR_MSG_PASSWORD, "password error message not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            return False, exc

#Success login with Email Address
    @TestTemplate.test_func()
    def test_C1606(self):
        try:
            login_page = LoginPage(self._driver)
            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title not accordance"
            time.sleep(1)
            login_page.input_email(const.EMAIL)
            email_value = login_page.check_email_value()
            assert email_value == const.EMAIL, "value of email not accordance"

            login_page.input_password(const.PASSWORD)
            login_page.click_password_icon()
            time.sleep(1)
            password_value = login_page.check_password_value()
            assert password_value == const.PASSWORD, "value of password not accordance"
            login_page.click_password_icon()

            login_page.click_login_button()
            username = login_page.username_text()
            assert username == "Hi, "+const.NAME, "Username not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#Without Activate Account
    @TestTemplate.test_func()
    def test_C6655(self):
        try:
            login_page = LoginPage(self._driver)

            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title text not accordance"
            time.sleep(1)

            login_page.input_email(const.UNVERIFIED_EMAIL)
            email_value = login_page.check_email_value()
            assert email_value == const.UNVERIFIED_EMAIL, "value of email not accordance"

            login_page.input_password(const.PASSWORD)
            login_page.click_password_icon()
            time.sleep(1)
            password_value = login_page.check_password_value()
            assert password_value == const.PASSWORD, "value of password not accordance"
            login_page.click_password_icon()

            login_page.click_login_button()

            activation_msg = login_page.check_emsg_value()
            assert activation_msg == const.MSG_NOT_VERIFIED_ACCNT, "reminder message for activation not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#wrong format email
    @TestTemplate.test_func()
    def test_C7958(self):
        try:
            login_page = LoginPage(self._driver)

            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title text not accordance"
            time.sleep(1)

            login_page.input_email(const.WRONG_FORMAT_EMAIL)
            email_value = login_page.check_email_value()
            assert email_value == const.WRONG_FORMAT_EMAIL, "value of email not accordance"

            error_emsg = login_page.check_error_msg_email()
            assert error_emsg == const.ERROR_MSG_EMAIL, "message for wrong format email not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#wrong password
    @TestTemplate.test_func()
    def test_C1594(self):
        try:
            login_page = LoginPage(self._driver)

            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title text not accordance"
            time.sleep(1)

            login_page.input_email(const.EMAIL)
            email_value = login_page.check_email_value()
            assert email_value == const.EMAIL, "value of email not accordance"

            login_page.input_password(const.WRONG_PASSWORD)
            login_page.click_login_button()

            error_emsg = login_page.check_emsg_value()
            assert error_emsg == const.ERROR_WRONG_PASSWORD, "error message for wrong password not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# leave blank password field
    @TestTemplate.test_func()
    def test_C6811(self):
        try:
            login_page = LoginPage(self._driver)

            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title text not accordance"
            time.sleep(1)

            login_page.input_email(const.EMAIL)
            email_value = login_page.check_email_value()
            assert email_value == const.EMAIL, "value of email not accordance"

            login_page.input_password("")
            login_page.click_login_button()
            time.sleep(1)

            error_emsg = login_page.check_emsg_value()
            assert error_emsg == const.ERROR_BLANK_PASSWORD, "error message for blank password not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#Email Not Register
    @TestTemplate.test_func()
    def test_C4157(self):
        try:
            login_page = LoginPage(self._driver)
            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title text not accordance"
            time.sleep(1)
            login_page.input_email(const.EMAIL_NOT_REGRIS)
            email_value = login_page.check_email_value()
            assert email_value == const.EMAIL_NOT_REGRIS, "value of email not accordance"

            login_page.input_password(const.PASSWORD)
            login_page.click_password_icon()
            time.sleep(1)
            password_value = login_page.check_password_value()
            assert password_value == const.PASSWORD, "value of password not accordance"
            login_page.click_password_icon()

            login_page.click_login_button()
            error_emsg = login_page.check_emsg_value()
            assert error_emsg == const.MSG_EMAIL_NOT_REGISTERED, "error message for email not register not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#wrong format password 
    @TestTemplate.test_func()
    def test_C6657(self):
        try:
            login_page = LoginPage(self._driver)
            title = login_page.go_to_login_page()
            assert title == const.LOGIN_TITLE, "Title text not accordance"
            time.sleep(1)
            login_page.input_email(const.EMAIL)
            email_value = login_page.check_email_value()
            assert email_value == const.EMAIL, "value of email not accordance"

            login_page.input_password(const.WRONG_FORMAT_PASSWORD)
            login_page.click_login_button()
            time.sleep(1)
            error_emsg = login_page.check_emsg_value()
            assert error_emsg == const.MSG_MIN_PASSWORD, "message for wrong format password not accordance"
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc